import endpoint from '@/api/server';
const api = endpoint();

const state = {
    materials: [],
};

const actions = {
    loadMaterials ( { commit } ) {
        return api.get( '/materials?_limit=10&_sort=createdAt:DESC' ).then( res => {
            commit( 'setMaterials', res.data)
        } );
    },

    registerMaterial( context, data ){
        return api.post( '/materials', data );
    },

    downMaterial( { dispatch } , id ){
        return api.put( `/materials/${id}`, { status: false} ).finally( () => {
            dispatch( "loadMaterials" )
        });
    }
};

const mutations = {
    setMaterials ( state, data ) {
        state.materials = data;
    },
}

export default {
    namespaced: true,
    state,
    actions,
    mutations,
}