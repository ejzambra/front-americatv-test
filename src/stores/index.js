import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex);

import storecontrol from './modules/storecontrol';

const store = new Vuex.Store( {
    modules: {
        storecontrol,
    }
} );

export default store