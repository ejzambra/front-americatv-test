import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    scrollBehavior: function() {
        return { x: 0, y: 0 }
    },
    routes: [
        // Public
        {
            path: '/',
            name: 'material',
            component: () => import('@/views/MaterialPage.vue'),
        },
        {
            path: '/material-setting',
            name: 'material-setting',
            component: () => import('@/views/MateriaSettingPage.vue'),
        },
        {
            path: '/programas',
            name: 'programas',
            component: () => import('@/views/OtherPage.vue'),
        },
    ]
} );

export default router