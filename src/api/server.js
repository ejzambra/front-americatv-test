import axios from 'axios'

export default() => {
    const http = axios.create( {
        baseURL: process.env.VUE_APP_API,
        withCredentials: false,
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
    } );

    return http
}